annual_salary = int(input('Enter your annual salary: '))
portion_saved = float(input('Enter the percent of your salary to save, as a decimal: '))
total_cost = int(input('Enter the cost of your dream home: '))
down_payment = total_cost * 0.25
current_savings = 0.0
number_of_months = 0
annual_salary_per_month = annual_salary / 12
while current_savings <= down_payment:
    number_of_months += 1
    current_savings += current_savings * 0.04 / 12
    current_savings += annual_salary_per_month * portion_saved
print(number_of_months)